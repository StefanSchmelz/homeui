const express = require('express');
const path = require('path');
const { expr } = require('jquery');

const app = express();
const port = 3000;

app.get('/', (req, res) => {
  res.sendFile(path.join(__dirname + '/index.html')); 
});

app.use('/modules', express.static('node_modules'));
app.use('/riot', express.static('riot'));
app.use('/js', express.static('js'));
app.use('/assets', express.static('assets'));

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`);
});