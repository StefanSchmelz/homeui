import * as riot from 'riot'
import App from '../riot/app.riot'
import NavBar from '../riot/navbar.riot'

const mountApp = riot.component(App);

const app = mountApp(
  document.getElementById('content'),
  { message: 'Hello World' }
);
const navbar = riot.mount('navbar');
